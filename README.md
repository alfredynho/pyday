sudo su postgres
psql -c "DROP DATABASE pyday_app"
psql -c "DROP USER pyday_user"
psql -c "CREATE USER pyday_user WITH ENCRYPTED PASSWORD 'XXXYYYZZZ'"
psql -c "CREATE DATABASE pyday_app WITH OWNER pyday_user"

