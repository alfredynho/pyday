# Generated by Django 2.2.3 on 2019-07-12 15:59

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('speaker', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='speaker',
            name='presentation',
            field=models.CharField(max_length=100, verbose_name='Tipo de Ponencia # Charla , Taller ...'),
        ),
        migrations.DeleteModel(
            name='PresentationType',
        ),
    ]
