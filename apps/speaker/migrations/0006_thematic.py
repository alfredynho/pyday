# Generated by Django 2.2.3 on 2019-07-15 19:06

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('speaker', '0005_auto_20190715_1155'),
    ]

    operations = [
        migrations.CreateModel(
            name='Thematic',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150, verbose_name='Nombre de la tematica')),
                ('percentage', models.IntegerField(default=17, verbose_name='Porcentaje')),
            ],
        ),
    ]
