# Generated by Django 2.2.3 on 2019-07-15 23:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('speaker', '0011_speaker_is_organizer'),
    ]

    operations = [
        migrations.CreateModel(
            name='Statistic',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=150, verbose_name='Nombre de la actividad')),
                ('percentage', models.IntegerField(default=10, verbose_name='Valor numerico')),
            ],
        ),
        migrations.AlterField(
            model_name='thematic',
            name='percentage',
            field=models.IntegerField(default=50, verbose_name='Porcentaje'),
        ),
    ]
