from django.shortcuts import render
from django.views.generic import TemplateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

from apps.speaker.models import Speaker


class DetailSpeakerView(DetailView):
    model = Speaker
    template_name = "detail.html"
    context_object_name = "speaker"