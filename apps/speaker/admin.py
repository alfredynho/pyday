from django.contrib import admin
from apps.speaker.models import Speaker, Thematic, Statistic


@admin.register(Speaker)
class SpeakerAdmin(admin.ModelAdmin):

    list_display = ('display_name_speaker','area','topic', 'level', 'exhibition_time')

    list_filter = ('first_name', 'last_name', 'area')

    fieldsets = (
        ('Datos Personales', {
            'fields': (
                'first_name',
                'last_name',
                'social_linkedin',
                'social_twitter',
                'social_facebook',
                'biography',
                'photo',
                'institution',
                'presentation',
                'slug',
                'is_workshop',
                'is_organizer'
            )
        }),
        ('Exposición/Charlar - Taller', {
            'fields': (
                'area', 
                'topic',
                'description_of_the_topic',
                'level',
                'exhibition_time'
            )
        }),
    )
    class Meta:
        model = Speaker
        
        
@admin.register(Thematic)
class ThematicAdmin(admin.ModelAdmin):
    
    list_display = (
        'name',
        'percentage'
    )
    
    class Meta:
        model = Thematic
        
        
@admin.register(Statistic)
class StatisticAdmin(admin.ModelAdmin):
    
    list_display = (
        'name',
        'percentage'
    )
    
    class Meta:
        model = Statistic