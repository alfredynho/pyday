from django.db import models
from django.utils.text import slugify


class Speaker(models.Model):

    first_name = models.CharField(
        max_length = 100,
        verbose_name = "Nombre"
    )

    last_name = models.CharField(
        max_length = 100,
        verbose_name = "Apellidos"
    )

    social_linkedin = models.CharField(
        max_length = 255,
        blank = True,
        null = True,
        verbose_name = "Linkedin"        
    )

    social_twitter = models.CharField(
        blank = True,
        null = True,
        max_length = 255,
        verbose_name = "Twitter"        
    )

    social_facebook = models.CharField(
        blank = True,
        null = True,
        max_length = 255,
        verbose_name = "Facebook"        
    )

    biography = models.TextField(
        verbose_name = "Biografia del Expositor" 
    )

    photo = models.ImageField(
        upload_to = "images/speakers",
        blank=True, 
        null=True
    )

    slug = models.SlugField(
        verbose_name = "Slug Autogenerado por el Backend",
        blank = True,
        null = True
    )
    
    institution = models.CharField(
        max_length = 100,
        verbose_name = "Intitución , Empresa , Comunidad"      
    )

    # Seccion de Exposicion, Taller, Charla 

    presentation = models.CharField(
        max_length = 100,
        verbose_name = "Tipo de Ponencia # Charla , Taller ..." 
    )

    area = models.CharField(
        max_length = 100,
        verbose_name = "area # Hacking, Devops, Web, ML ..." 
    )

    topic = models.CharField(
        max_length = 100,
        verbose_name = "Tema"
    )

    description_of_the_topic = models.TextField(
        verbose_name = "Descripcion de la #Charla, Taller ... "
    )

    level = models.CharField(
        max_length = 100,
        verbose_name = "Nivel #Basico, Medio Avanzado"
    )

    exhibition_time = models.DateTimeField(
        verbose_name = "hora de #Charla, Taller ..."
    )

    is_workshop = models.BooleanField(
        default = False,
        verbose_name = "Es Taller?"
    )
    
    is_organizer = models.BooleanField(
        default = False,
        verbose_name = "Es organizador?"
    )


    def display_name_speaker(self):
        return "%s %s " %(self.first_name, self.last_name)
    display_name_speaker.short_description = 'Nombre Completo'


    def _get_unique_slug(self):
        slug = slugify(self.topic)
        unique_slug = slug
        num = 1
        while Speaker.objects.filter(slug=unique_slug).exists():
            unique_slug = '{}-{}'.format(slug, num)
            num += 1
        return unique_slug


    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = self._get_unique_slug()
        super().save(*args, **kwargs)
        
        
        
class Thematic(models.Model):
    
    name = models.CharField(
        max_length = 150,
        verbose_name = "Nombre de la tematica"    
    )
    
    percentage = models.IntegerField(
        verbose_name = "Porcentaje",
        default = 50
    )
    
    right_position = models.BooleanField(
        default = False,
        verbose_name = "Pos derecha = True, Pos Izquierda = False "
    )
    
    
class Statistic(models.Model):
    
    name = models.CharField(
        max_length = 150,
        verbose_name = "Nombre de la actividad"    
    )
    
    percentage = models.IntegerField(
        verbose_name = "Valor numerico",
        default = 10
    )
    
    icon = models.CharField(
        max_length = 50,
        verbose_name = "Icono"
    )
    