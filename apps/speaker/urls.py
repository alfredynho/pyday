from django.urls import path
from django.conf.urls import url

from .views import DetailSpeakerView

urlpatterns = [
    url(r'^speaker-detail/(?P<slug>[\w-]+)/$', DetailSpeakerView.as_view(), name='speaker-detail'),
]