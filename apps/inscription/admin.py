from django.contrib import admin
from .models import Tipo,TipoInscripcion ,Inscrito
from import_export.admin import ImportExportModelAdmin


@admin.register(Tipo)
class AdminTipo(admin.ModelAdmin):

    list_display = [
        'name'
    ]

    class Meta:
        model = Tipo


@admin.register(TipoInscripcion)
class AdminTipoInscripcion(admin.ModelAdmin):

    list_display = [
        'inscripcion',
        'name',
        'quota'
    ]

    class Meta:
        model = TipoInscripcion

class AdminInscrito(admin.ModelAdmin):

    list_display = [
        'name',
        'email',
        'phone',
        'age',
        'gender',
        'institution',
        'level',
        'tipo',
        'tipoinscripcion'
    ]

    class Meta:
        model = Inscrito
        
        
@admin.register(Inscrito)
class InscritoAdmin(ImportExportModelAdmin):
    pass