from django import forms
from .models import Inscrito, TipoInscripcion


class PersonForm(forms.ModelForm):
    class Meta:

        model = Inscrito

        fields = (
            'name',
            'email',
            'phone',
            'age',
            'gender',
            'institution',
            'level',
            'tipo',
            'tipoinscripcion'
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['tipoinscripcion'].queryset = TipoInscripcion.objects.none()

        if 'tipo' in self.data:
            try:
                tipo_id = int(self.data.get('tipo'))

                self.fields['tipoinscripcion'].queryset = TipoInscripcion.objects.filter(inscripcion_id=tipo_id).order_by('name')
            except (ValueError, TypeError):
                pass
        elif self.instance.pk:
            self.fields['tipoinscripcion'].queryset = self.instance.tipo.tipoinscripcion_set.order_by('name')
