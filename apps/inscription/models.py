from django.db import models

class Tipo(models.Model):

    name = models.CharField(
        max_length=150,
        verbose_name = "Charla o Taller --> Categoria"
    )
    
    def __str__(self):
        return self.name


class TipoInscripcion(models.Model):

    inscripcion = models.ForeignKey(
        Tipo, on_delete=models.CASCADE
    )
    
    name = models.CharField(
        max_length = 150,
        verbose_name = "El nombre de la Charla o Taller"
    )

    quota = models.IntegerField(
        verbose_name = "Cupo"
    )

    def __str__(self):
        return self.name



VARON = 'V'
MUJER = 'M'
OTRO = 'O'

AGE_CHOICES = [
    ('M', 'Mujer'),
    ('V', 'Varon'),
    ('O', 'Otro'),
]


class Inscrito(models.Model):

    name = models.CharField(
        max_length=100,
        verbose_name = "Nombre completo del participante"
    )

    email = models.EmailField(
        verbose_name = "Correo"
    )

    phone = models.IntegerField(
        verbose_name = "Número de Celular"
    )

    age = models.IntegerField(
        verbose_name = "Edad",
        blank = True,
        null = True
    )

    gender = models.CharField(
        max_length = 4,
        choices = AGE_CHOICES,
        default = VARON,
        verbose_name = "Género"
    )

    institution = models.CharField(
        max_length = 200,
        verbose_name = "Institución Educativa/ Empresa/Otros"
    )

    level = models.CharField(
        max_length = 200,
        verbose_name = "Nivel/Año que cursa (Colegio/Universidad)",
        blank = True,
        null = True
    )

    tipo = models.ForeignKey(
        Tipo, on_delete = models.SET_NULL, 
        null = True,
        blank = True,
        verbose_name = "Tipo"
    )

    tipoinscripcion = models.ForeignKey(
        TipoInscripcion, on_delete=models.SET_NULL,
        null = True,
        blank = True,
        verbose_name = "Categoria"
    )

    def __str__(self):
        return "%s /  %s " %(self.name,self.tipo)

