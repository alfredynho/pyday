from django.shortcuts import render
from django.views.generic import ListView, CreateView, UpdateView
from django.views.generic.base import TemplateView

from django.urls import reverse_lazy

from .models import Inscrito, TipoInscripcion
from .forms import PersonForm
from constance import config


class FormInscriptionView(CreateView):
    
    model = Inscrito
    form_class = PersonForm
    success_url = reverse_lazy('inscription:registro')

    
    
def load_tematic(request):
    
    tipo_id = request.GET.get('tipo')
    tematics = TipoInscripcion.objects.filter(inscripcion_id=tipo_id).order_by('name')

    return render(request, 'inscription/list_options.html', {'tematics': tematics})


class MessageSuccessFormView(TemplateView):
    
    template_name = 'inscription/message_registration.html'
    
    def get_context_data(self, **kwargs):
        context = super(MessageSuccessFormView, self).get_context_data(**kwargs)
        context['message_form'] = config.MESSAGE_FORM
        return context