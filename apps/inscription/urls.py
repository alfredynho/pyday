from django.urls import path
from django.conf.urls import url

from .views import FormInscriptionView, MessageSuccessFormView
from . import views


urlpatterns = [
    path('formulario_inscripcion', FormInscriptionView.as_view(),name='formulario'),
    path('ajax/load-cities/', views.load_tematic, name='ajax_load_tematic'),
    path('registro_exitoso', MessageSuccessFormView.as_view(), name='registro'),
]