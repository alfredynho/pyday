from django.shortcuts import render
from django.views.generic import TemplateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView

from django.shortcuts import render
from django.http import HttpResponse

from apps.speaker.models import Speaker, Thematic, Statistic

class IndexView(ListView):
    template_name = "index.html"    
    model = Speaker
            
    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context['speaker'] = Speaker.objects.all()
        context['thematic'] = Thematic.objects.all()
        context['workshop'] = Speaker.objects.all().filter(is_workshop = True)           
        context['organizer'] = Speaker.objects.all().filter(is_organizer = True)           
        context['statistics'] = Statistic.objects.all()
        return context

class AuspiciadoresView(TemplateView):
    template_name = "auspiciadores.html"


class WorkShopsView(ListView):
    template_name = "workshops.html"    
    model = Speaker
            
    def get_context_data(self, **kwargs):
        context = super(WorkShopsView, self).get_context_data(**kwargs)
        context['workshop'] = Speaker.objects.all().filter(is_workshop = True)           
        return context


class CharlasView(ListView):
    template_name = "charlas.html"    
    model = Speaker
            
    def get_context_data(self, **kwargs):
        context = super(CharlasView, self).get_context_data(**kwargs)
        context['charlas'] = Speaker.objects.all().filter(is_workshop = False)           
        return context


def error_404_view(request, exception):
    data = {"name": "pythonbolivia.org"}
    return render(request,'error_404.html', data)