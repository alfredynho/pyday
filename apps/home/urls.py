
from django.conf.urls import url
from django.urls import path, include

from .views import IndexView, DetailView, AuspiciadoresView, WorkShopsView, CharlasView


urlpatterns = [
    path('', IndexView.as_view(),name='index'),
    path('auspiciadores', AuspiciadoresView.as_view(),name='auspiciadores'),
    path('workshops', WorkShopsView.as_view(),name='workshops'),
    path('charlas', CharlasView.as_view(),name='charlas'),
]

