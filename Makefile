run:
	@echo "------------------------> Reising Server <------------------------"
	python manage.py runserver

createuser:
	python manage.py createsuperuser --username=admin --email=admin@example.com


shell:
	python manage.py shell


migrate:
	python manage.py makemigrations
	python manage.py migrate


collect:
	python manage.py collectstatic



install:
	virtualenv -p python3 env
	$(PIP_ENV) install -r requirements/common.txt	
	npm install


prod:
	pip install -r requirements/production.txt



constance:
	python manage.py constance


options:
	@echo
	@echo ----------------------------------------------------------------------
	@echo "   >>>>>                 Openbackend               <<<<<   "
	@echo ----------------------------------------------------------------------
	@echo
	@echo "   - install     SETTINGS=[settings]    Install App and their dependencies"
	@echo "   - superuser   SETTINGS=[settings]    Create a super user in production"
	@echo "   - serve       SETTINGS=[settings]    Serve project for development"
	@echo "   - mail_server SETTINGS=[settings]    Open the Development Mail Server"
	@echo "   - shell       SETTINGS=[settings]    Run Django in shell mode for development"
	@echo "   - test        SETTINGS=[settings]    Run Django test cases"
	@echo "   - constance   SETTINGS=[settings]    settings django contance"
	@echo
	@echo ----------------------------------------------------------------------

clean:
	rm -rf node_modules
	rm -rf static/dist

lint:
	@npm run lint --silent

