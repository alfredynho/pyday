
from django.contrib import admin
from django.urls import path
from django.conf.urls import url, include

from django.conf import settings

from django.contrib.staticfiles.urls import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls import handler404


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include(('apps.home.urls','home'),namespace='home')),
    path('', include(('apps.speaker.urls','speakers'),namespace='speakers')),
    path('', include(('apps.inscription.urls','inscription'),namespace='inscription')),
]

handler404 = 'apps.home.views.error_404_view'

urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)